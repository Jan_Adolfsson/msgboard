﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MsgBoard.Api.Dtos;
using MsgBoard.Core.Models;
using MsgBoard.Core.Interfaces.Services;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using MsgBoard.Api.Extensions;

namespace MsgBoard.Api.Controllers
{
    [ApiController]
    [Authorize]
    [Route("api/[controller]")]
    public class MessagesController : ControllerBase
    {
        private readonly ILogger<MessagesController> _logger;
        private readonly IMapper _mapper;
        private readonly IMsgService _msgService;

        public MessagesController(
            ILogger<MessagesController> logger, 
            IMapper mapper, 
            IMsgService msgService)
        {
            _logger = logger;
            _mapper = mapper;
            _msgService = msgService;
        }

        /// <summary>
        /// Create a new message with authenticated user as author.
        /// </summary>
        /// <param name="msg">The new message.</param>
        /// <returns>Returns id of new message.</returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Post([FromBody]MessageUpdate msg)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState.GetErrors());
            }

            var msgModel = _mapper.Map<MessageModel>(msg);
            msgModel.UserId = this.GetUserId();

            var svcResp = await _msgService.Add(msgModel);
            if (!svcResp.Success)
                return BadRequest(svcResp.Error);

            return CreatedAtAction(nameof(GetById), new { id = svcResp.Data }, svcResp.Data);
        }

        /// <summary>
        /// Update existing message.
        /// </summary>
        /// <param name="id">Id of message to update.</param>
        /// <param name="msg">Updated message.</param>
        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Put(int id, [FromBody]MessageUpdate msg)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState.GetErrors());
            }
            
            var msgModel = _mapper.Map<MessageModel>(msg);
            msgModel.Id = id;

            var svcResp = await _msgService.Update(this.GetUserId(), msgModel);
            if (!svcResp.Success)
                return BadRequest(svcResp.Error);

            return NoContent();
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Delete(int id)
        {
            var svcResp = await _msgService.Delete(this.GetUserId(), id);
            if (!svcResp.Success)
                return BadRequest(svcResp.Error);

            return NoContent();
        }


        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> GetAll()
        {
            var svcResp = await _msgService.GetAll();
            if (!svcResp.Success)
                return BadRequest(svcResp.Error);

            var msgs = _mapper.Map<IEnumerable<Message>>(svcResp.Data);
            return Ok(msgs);
        }

        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> GetById(int id)
        {
            var svcResp = await _msgService.Get(id);
            if (!svcResp.Success)
                return BadRequest(svcResp.Error);

            var msg = _mapper.Map<Message>(svcResp.Data);
            return Ok(msg);
        }

        [HttpGet("~/api/users/{userid}/messages")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> GetMsgsByUserId(int userId)
        {
            var svcResp = await _msgService.GetByUserId(userId);
            if (!svcResp.Success)
            {
                return BadRequest(svcResp.Error);
            }

            var msgs = _mapper.Map<IEnumerable<Message>>(svcResp.Data);
            return Ok(msgs);
        }
    }
}
