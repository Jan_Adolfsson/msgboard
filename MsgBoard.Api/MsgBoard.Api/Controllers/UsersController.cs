﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MsgBoard.Api.Dtos;
using MsgBoard.Api.Extensions;
using MsgBoard.Api.Helpers;
using MsgBoard.Api.Settings;
using MsgBoard.Core.Interfaces.Services;
using MsgBoard.Core.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MsgBoard.Api.Controllers
{
    [ApiController]
    [Authorize]
    [Route("api/[controller]")]
    public class UsersController : ControllerBase
    {
        private readonly ILogger<UsersController> _logger;
        private readonly AuthSettings _authSettings;
        private readonly IMapper _mapper;
        private readonly IUserService _userService;
        private readonly IMsgService _msgService;

        public UsersController(
            ILogger<UsersController> logger, 
            IOptions<AuthSettings> authSettings,
            IMapper mapper, 
            IUserService userService, 
            IMsgService msgService)
        {
            _logger = logger;
            _authSettings = authSettings.Value;
            _mapper = mapper;
            _userService = userService;
            _msgService = msgService;
        }

        [AllowAnonymous]
        [HttpPost("authenticate")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Authenticate([FromBody]Authentication authentication)
        {
            var svcResp = await _userService.Authenticate(authentication.Username, authentication.Password);
            if(!svcResp.Success)
            {
                return BadRequest(svcResp.Error);
            }

            var user = svcResp.Data;
            var token = TokenBuilder.Build(user.Id, _authSettings.Secret, _authSettings.TokenLifetime);

            var authResp = new AuthenticateResponse
            {
                Token = token,
                User = new User
                { 
                    Id = user.Id,
                    Username = user.Username 
                }
            };

            return Ok(authResp);
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> GetAll()
        {
            var svcResp = await _userService.GetAll();
            if (!svcResp.Success)
            {
                return BadRequest(svcResp.Error);
            }

            var users = _mapper.Map<IEnumerable<User>>(svcResp.Data);
            return Ok(users);
        }

        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> GetById(int id)
        {
            var svcResp = await _userService.Get(id);
            if (!svcResp.Success)
                return BadRequest(svcResp.Error);

            var user = _mapper.Map<User>(svcResp.Data);
            return Ok(user);
        }


        /// <summary>
        /// Create a new user.
        /// </summary>
        /// <param name="user">The new user.</param>
        /// <returns>Returns id of new user.</returns>
        [HttpPost]
        [AllowAnonymous]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Post([FromBody]UserUpdate user)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState.GetErrors());
            }

            var userModel = _mapper.Map<UserModel>(user);
            var svcResp = await _userService.Add(userModel);
            if (!svcResp.Success)
            {
                return BadRequest(svcResp.Error);
            }

            return CreatedAtAction(nameof(GetById),new { id= svcResp.Data }, svcResp.Data);
        }
    }
}
