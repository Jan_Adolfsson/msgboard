﻿using System.ComponentModel.DataAnnotations;

namespace MsgBoard.Api.Dtos
{
    public class Message
    {
        public int UserId { get; set; }
        public int Id { get; set; }
        public string Head { get; set; }
        public string Body { get; set; }
    }
}
