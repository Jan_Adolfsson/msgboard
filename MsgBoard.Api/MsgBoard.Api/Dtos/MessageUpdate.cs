﻿using System.ComponentModel.DataAnnotations;

namespace MsgBoard.Api.Dtos
{
    public class MessageUpdate
    {
        [Required]
        [StringLength(50)]
        public string Head { get; set; }
        
        [Required]
        [StringLength(500)]
        public string Body { get; set; }
    }
}
