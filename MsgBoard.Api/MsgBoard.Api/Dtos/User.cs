﻿using System.ComponentModel.DataAnnotations;

namespace MsgBoard.Api.Dtos
{
    public class User
    {
        public int Id { get; set; }
        public string Username { get; set; }
    }
}
