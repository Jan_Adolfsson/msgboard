﻿using System.ComponentModel.DataAnnotations;

namespace MsgBoard.Api.Dtos
{
    public class UserUpdate
    {
        [Required]
        [StringLength(25)]
        public string Username { get; set; }

        [Required]
        [StringLength(16, MinimumLength = 8)]
        public string Password { get; set; }
    }
}
