﻿using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Security.Claims;

namespace MsgBoard.Api.Extensions
{
    public static class ControllerExtensions
    {
        public static int GetUserId(this ControllerBase controller)
        {
            var claims = controller
                .HttpContext
                .User
                .Claims;
            var nameClaim = claims
                .Single(claim => claim.Type == ClaimTypes.Name);

            var userId = int.Parse(nameClaim.Value);
            return userId;
        }
    }
}
