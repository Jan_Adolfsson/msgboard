﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System.Collections.Generic;
using System.Linq;

namespace MsgBoard.Api.Extensions
{
    public static class ModelStateDictionaryExtensions
    {
        public static IEnumerable<string> GetErrors(this ModelStateDictionary modelState)
        {
            return modelState
                .SelectMany(state => state.Value.Errors)
                .Select(err => err.ErrorMessage);
        }
    }
}
