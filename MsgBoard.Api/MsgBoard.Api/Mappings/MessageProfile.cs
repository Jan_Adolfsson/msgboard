﻿using AutoMapper;
using MsgBoard.Api.Dtos;
using MsgBoard.Core.Models;

namespace MsgBoard.Api.Mappings
{
    public class MessageProfile : Profile
    {
        public MessageProfile()
        {
            CreateMap<MessageUpdate, MessageModel>();
            CreateMap<MessageModel, Message>();
        }
    }
}
