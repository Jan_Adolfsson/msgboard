﻿using AutoMapper;
using MsgBoard.Api.Dtos;
using MsgBoard.Core.Models;

namespace MsgBoard.Api.Mappings
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<User, UserModel>();
            CreateMap<UserModel, User>();
            CreateMap<UserUpdate, UserModel>();
        }
    }
}
