﻿namespace MsgBoard.Api.Settings
{
    public class AuthSettings
    {
        public string Secret { get; set; }
        public int TokenLifetime { get; set; }
    }
}
