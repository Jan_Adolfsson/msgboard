﻿using System.Text;
using System.Security.Cryptography;
using System;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("MyTests")]

namespace MsgBoard.Core.Helpers
{
    static class PasswordHasher
    {
        internal static (byte[] hash, byte[] salt) Create(string password)
        {
            byte[] hash, salt;
            using (var hasher = new HMACSHA512())
            {
                salt = hasher.Key;
                hash = hasher.ComputeHash(Encoding.UTF8.GetBytes(password));
            }

            return (hash, salt);
        }

        internal static bool Verify(string password, byte[] hash, byte[] salt)
        {
            if (hash.Length != 64)
            {
                throw new ArgumentException("Invalid length of hash, expected 64 bytes.", nameof(hash));
            }
            if (salt.Length != 128)
            {
                throw new ArgumentException("Invalid length of salt, expected 128 bytes.", nameof(salt));
            }

            using (var hasher = new HMACSHA512(salt))
            {
                var computedHash = hasher.ComputeHash(Encoding.UTF8.GetBytes(password));
                for (int i = 0; i < computedHash.Length; i++)
                {
                    if (computedHash[i] != hash[i])
                    {
                        return false;
                    }
                }
            }

            return true;
        }
    }
}

