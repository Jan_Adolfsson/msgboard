﻿using MsgBoard.Core.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MsgBoard.Core.Interfaces.Repositories
{
    public interface IMsgRepository
    {
        Task<int> Add(MessageModel msg);
        Task Delete(MessageModel msg);
        Task<IEnumerable<MessageModel>> GetAll();
        Task<MessageModel> Get(int id);
        Task<IEnumerable<MessageModel>> GetByUserId(int userId);
        Task Update(MessageModel msg);
    }
}
