﻿using MsgBoard.Core.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MsgBoard.Core.Interfaces.Repositories
{
    public interface IUserRepository
    {
        Task<int> Add(UserModel user);
        Task<UserModel> Get(string username);
        Task<UserModel> Get(int id);
        Task<IEnumerable<UserModel>> GetAll();
    }
}
