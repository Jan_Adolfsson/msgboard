﻿using MsgBoard.Core.Models;
using MsgBoard.Core.Responses;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MsgBoard.Core.Interfaces.Services
{
    public interface IMsgService
    {
        Task<DataResponse<int>> Add(MessageModel msg);
        Task<VoidResponse> Delete(int userId, int id);
        Task<VoidResponse> Update(int userId, MessageModel msg);
        Task<DataResponse<MessageModel>> Get(int id);
        Task<DataResponse<IEnumerable<MessageModel>>> GetAll();
        Task<DataResponse<IEnumerable<MessageModel>>> GetByUserId(int userId);
    }
}
