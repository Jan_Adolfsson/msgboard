﻿using MsgBoard.Core.Models;
using MsgBoard.Core.Responses;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MsgBoard.Core.Interfaces.Services
{
    public interface IUserService
    {
        public Task<DataResponse<int>> Add(UserModel user);
        public Task<DataResponse<UserModel>> Authenticate(string username, string password);
        public Task<DataResponse<UserModel>> Get(int id);
        public Task<DataResponse<IEnumerable<UserModel>>> GetAll();
    }
}
