﻿namespace MsgBoard.Core.Models
{
    public class MessageModel
    {
        public int Id { get; set; }
        public string Head { get; set; }
        public string Body { get; set; }
        public int UserId { get; set; }
    }
}
