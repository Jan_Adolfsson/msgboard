﻿namespace MsgBoard.Core.Responses
{
    public abstract class BaseResponse
    {
        public string Error { get; }
        public bool Success { get; }
        protected BaseResponse(bool success = true, string errorMsg = null)
        {
            Success = success;
            Error = errorMsg;
        }
    }
}
