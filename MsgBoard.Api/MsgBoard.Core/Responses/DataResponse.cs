﻿using MsgBoard.Core.Models;

namespace MsgBoard.Core.Responses
{
    public class DataResponse<T>:BaseResponse
    {
        public T Data { get; private set; }
        public DataResponse(string error):base(false, error)
        {}
        public DataResponse(T data)
        {
            Data = data;
        }
    }

}
