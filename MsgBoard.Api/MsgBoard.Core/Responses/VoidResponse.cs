﻿namespace MsgBoard.Core.Responses
{
    public class VoidResponse : BaseResponse
    {
        public VoidResponse(string error) : base(false, error)
        { }
        public VoidResponse()
        { }
    }
}
