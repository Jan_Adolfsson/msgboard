﻿using MsgBoard.Core.Models;
using MsgBoard.Core.Interfaces.Repositories;
using MsgBoard.Core.Responses;
using MsgBoard.Core.Interfaces.Services;
using System;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace MsgBoard.Core.Services
{
    public class MsgService : IMsgService
    {
        private readonly IMsgRepository _msgRepo;

        public MsgService(IMsgRepository msgRepo)
        {
            _msgRepo = msgRepo;
        }

        public async Task<DataResponse<int>> Add(MessageModel msg)
        {
            try
            {
                return new DataResponse<int>(await _msgRepo.Add(msg));
            }
            catch (Exception ex)
            {
                return new DataResponse<int>(ex.Message);
            }
        }

        public async Task<VoidResponse> Delete(int userId, int id)
        {
            try
            {
                var msg = await _msgRepo.Get(id);
                if(msg.UserId != userId)
                {
                    return new VoidResponse("You may only delete your own messages.");
                }

                await _msgRepo.Delete(msg);

                return new VoidResponse();
            }
            catch (Exception ex)
            {
                return new VoidResponse(ex.Message);
            }
        }

        public async Task<DataResponse<MessageModel>> Get(int id)
        {
            try
            {
                var msg = await _msgRepo.Get(id);
                if(msg == null)
                {
                    return new DataResponse<MessageModel>("Message doesn't exist.");
                }

                return new DataResponse<MessageModel>(msg);
            }
            catch (Exception ex)
            {
                return new DataResponse<MessageModel>(ex.Message);
            }
        }

        public async Task<DataResponse<IEnumerable<MessageModel>>> GetAll()
        {
            try
            {
                var msgs = await _msgRepo.GetAll();
                return new DataResponse<IEnumerable<MessageModel>>(msgs);
            }
            catch (Exception ex)
            {
                return new DataResponse<IEnumerable<MessageModel>>(ex.Message);
            }
        }

        public async Task<DataResponse<IEnumerable<MessageModel>>> GetByUserId(int userId)
        {
            try
            {
                var msgs = await _msgRepo.GetByUserId(userId);
                return new DataResponse<IEnumerable<MessageModel>>(msgs);
            }
            catch (Exception ex)
            {
                return new DataResponse<IEnumerable<MessageModel>>(ex.Message);
            }
        }

        public async Task<VoidResponse> Update(int userId, MessageModel msg)
        {
            try
            {
                var existingMsg = await _msgRepo.Get(msg.Id);
                if(existingMsg.UserId != userId)
                {
                    return new VoidResponse("You may only modify your own messages.");
                }


                existingMsg.Head = msg.Head;
                existingMsg.Body = msg.Body;
                await _msgRepo.Update(existingMsg);

                return new VoidResponse();
            }
            catch (Exception ex)
            {
                return new VoidResponse(ex.Message);
            }
        }
    }
}
