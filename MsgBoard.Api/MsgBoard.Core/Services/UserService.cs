﻿using MsgBoard.Core.Interfaces.Repositories;
using MsgBoard.Core.Responses;
using MsgBoard.Core.Interfaces.Services;
using System;
using System.Threading.Tasks;
using MsgBoard.Core.Models;
using System.Collections.Generic;
using MsgBoard.Core.Helpers;

namespace MsgBoard.Core.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;

        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public async Task<DataResponse<int>> Add(UserModel user)
        {
            try
            {
                if (await _userRepository.Get(user.Username) != null)
                {
                    return new DataResponse<int>("User with given username allready exists.");
                }

                (byte[] hash, byte[] salt) = PasswordHasher.Create(user.Password);
                user.PasswordHash = hash;
                user.PasswordSalt = salt;

                var id = await _userRepository.Add(user);
                return new DataResponse<int>(id);
            }
            catch (Exception ex)
            {
                return new DataResponse<int>(ex.Message);
            }
        }
        public async Task<DataResponse<UserModel>> Authenticate(string username, string password)
        {
            try
            {
                var user = await _userRepository.Get(username);
                if (user == null)
                {
                    return new DataResponse<UserModel>("Username doesn't exist.");
                }

                if (!PasswordHasher.Verify(password, user.PasswordHash, user.PasswordSalt))
                {
                    return new DataResponse<UserModel>("Wrong username and/or password.");
                }

                return new DataResponse<UserModel>(user);
            }
            catch (Exception ex)
            {
                return new DataResponse<UserModel>(ex.Message);
            }
        }
        public async Task<DataResponse<UserModel>> Get(int id)
        {
            try
            {
                var user = await _userRepository.Get(id);
                if(user == null)
                {
                    return new DataResponse<UserModel>("User doesn't exist.");
                }
                return new DataResponse<UserModel>(user);
            }
            catch (Exception ex)
            {
                return new DataResponse<UserModel>(ex.Message);
            }
        }

        public async Task<DataResponse<IEnumerable<UserModel>>> GetAll()
        {
            try
            {
                var users = await _userRepository.GetAll();
                return new DataResponse<IEnumerable<UserModel>>(users);
            }
            catch (Exception ex)
            {
                return new DataResponse<IEnumerable<UserModel>>(ex.Message);
            }
        }
    }
}
