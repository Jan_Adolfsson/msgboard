﻿using Microsoft.EntityFrameworkCore;
using MsgBoard.Core.Models;

namespace MsgBoard.Infrastructure.Repositories
{
    public class DbContextExt : DbContext
    {
        public DbSet<MessageModel> Msgs { get; set; }
        public DbSet<UserModel> Users { get; set; }
        public DbContextExt(DbContextOptions options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.Entity<UserModel>().ToTable("User");
            builder.Entity<UserModel>().HasKey(p => p.Id);
            builder.Entity<UserModel>().Property(p => p.Id).IsRequired().ValueGeneratedOnAdd();
            builder.Entity<UserModel>().HasAlternateKey(p => p.Username);
            builder.Entity<UserModel>().Property(p => p.Username).IsRequired().HasMaxLength(25);
            builder.Entity<UserModel>().Property(p => p.Password).IsRequired().HasMaxLength(15);
            builder.Entity<UserModel>().HasData
            (
                new UserModel { Id = 1, Username = "Palpatine", Password = "Sith4Evr" },
                new UserModel { Id = 2, Username = "BabyYoda", Password = "Jedi4Lfe" }
            );

            builder.Entity<MessageModel>().ToTable("Message");
            builder.Entity<MessageModel>().HasKey(p => p.Id);
            builder.Entity<MessageModel>().Property(p => p.Id).IsRequired().ValueGeneratedOnAdd();
            builder.Entity<MessageModel>().Property(p => p.Head).IsRequired().HasMaxLength(50);
            builder.Entity<MessageModel>().Property(p => p.Body).IsRequired().HasMaxLength(500);
            builder.Entity<MessageModel>().Property(p => p.UserId).IsRequired();


            builder.Entity<MessageModel>().HasData
            (
                new MessageModel { Id = 1, Head = "Wanted", Body = "Little green creature with strange powers.", UserId = 1 },
                new MessageModel { Id = 2, Head = "Help", Body = "In need of urgent protection.", UserId = 2 }
            );

        }
    }
}
