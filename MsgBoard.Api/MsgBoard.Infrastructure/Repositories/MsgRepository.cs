﻿using Microsoft.EntityFrameworkCore;
using MsgBoard.Core.Models;
using MsgBoard.Core.Interfaces.Repositories;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MsgBoard.Infrastructure.Repositories
{
    public class MsgRepository : IMsgRepository
    {
        private readonly DbContextExt _dbContext;

        public MsgRepository(DbContextExt dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<int> Add(MessageModel msg)
        {
            var entry = await _dbContext.Msgs.AddAsync(msg);
            await _dbContext.SaveChangesAsync();
            return entry.Entity.Id;
        }

        public async Task Delete(MessageModel msg)
        {
            _dbContext.Msgs.Remove(msg);
            await _dbContext.SaveChangesAsync();
        }

        public async Task<MessageModel> Get(int id)
        {
            return await _dbContext.Msgs.FindAsync(id);
        }

        public async Task<IEnumerable<MessageModel>> GetAll()
        {
            return await _dbContext.Msgs.ToListAsync();
        }

        public async Task<IEnumerable<MessageModel>> GetByUserId(int userId)
        {
            return await _dbContext.Msgs.Where(x => x.UserId == userId).ToListAsync();
        }

        public async Task Update(MessageModel msg)
        {
            _dbContext.Msgs.Update(msg);
            await _dbContext.SaveChangesAsync();
        }
    }
}
