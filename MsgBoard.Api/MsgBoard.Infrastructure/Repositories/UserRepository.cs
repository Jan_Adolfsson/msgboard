﻿using Microsoft.EntityFrameworkCore;
using MsgBoard.Core.Models;
using MsgBoard.Core.Interfaces.Repositories;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MsgBoard.Infrastructure.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly DbContextExt _dbContext;

        public UserRepository(DbContextExt dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<int> Add(UserModel user)
        {
            var entry = _dbContext.Users.Add(user);
            await _dbContext.SaveChangesAsync();
            return entry.Entity.Id;
        }

        public async Task<UserModel> Get(string username)
        {
            return await _dbContext.Users
                .SingleOrDefaultAsync(u => string.Compare(u.Username, username, true) == 0);
        }

        public async Task<UserModel> Get(int id)
        {
            return await _dbContext.Users.FindAsync(id);
        }

        public async Task<IEnumerable<UserModel>> GetAll()
        {
            return await _dbContext.Users.ToListAsync();
        }
    }
}
