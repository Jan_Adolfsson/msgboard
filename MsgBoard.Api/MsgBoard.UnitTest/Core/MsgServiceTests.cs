﻿using Moq;
using MsgBoard.Core.Interfaces.Repositories;
using MsgBoard.Core.Models;
using MsgBoard.Core.Services;
using Shouldly;
using System.Threading.Tasks;
using Xunit;

namespace MsgBoard.UnitTests.Core
{
    public class MsgServiceTests
    {
        public class Update
        {
            [Fact]
            public async Task Should_return_error_When_updating_somebody_elses_message()
            {
                // Arrange
                var msgRepoMock = new Mock<IMsgRepository>();
                msgRepoMock
                    .Setup(x => x.Get(1))
                    .ReturnsAsync(new MessageModel { Id = 1, UserId = 10 });

                var msgSvc = new MsgService(msgRepoMock.Object);

                // Act
                var response = await msgSvc.Update(9, new MessageModel { Id = 1 });

                // Assert
                response.Success.ShouldBeFalse();
                response.Error.ShouldBe("You may only modify your own messages.");
            }

            [Fact]
            public async Task Should_return_success_When_updating_own_message()
            {
                // Arrange
                var msgRepoMock = new Mock<IMsgRepository>();
                msgRepoMock
                    .Setup(x => x.Get(1))
                    .ReturnsAsync(new MessageModel { Id = 1, UserId = 2 });

                var msgSvc = new MsgService(msgRepoMock.Object);

                // Act
                var response = await msgSvc.Update(2, new MessageModel { Id = 1 });

                // Assert
                response.Success.ShouldBeTrue();
                response.Error.ShouldBeNull();
            }
            [Fact]
            public async Task Should_pass_updated_message_to_repo_When_updating_own_message()
            {
                // Arrange
                var msgRepoMock = new Mock<IMsgRepository>();
                msgRepoMock
                    .Setup(x => x.Get(1))
                    .ReturnsAsync(new MessageModel { Id = 1, UserId = 2 });

                var msgSvc = new MsgService(msgRepoMock.Object);

                // Act
                var msg = new MessageModel
                {
                    Id = 1,
                    UserId = 2,
                    Head = "Head",
                    Body = "Body"
                };
                var response = await msgSvc.Update(2, msg);

                // Assert
                msgRepoMock.Verify(mock => 
                    mock.Update(
                        It.Is<MessageModel>(mm => 
                            mm.Id == 1 && 
                            mm.UserId == 2 && 
                            mm.Head == "Head" && 
                            mm.Body == "Body")));
            }
        }
        public class Delete
        {
            [Fact]
            public async Task Should_return_error_When_deleting_somebody_elses_message()
            {
                // Arrange
                var msgRepoMock = new Mock<IMsgRepository>();
                msgRepoMock
                    .Setup(x => x.Get(1))
                    .ReturnsAsync(new MessageModel { Id = 1, UserId = 10 });

                var msgSvc = new MsgService(msgRepoMock.Object);

                // Act
                var response = await msgSvc.Delete(9, 1);

                // Assert
                response.Success.ShouldBeFalse();
                response.Error.ShouldBe("You may only delete your own messages.");
            }

            [Fact]
            public async Task Should_return_success_When_deleting_own_message()
            {
                // Arrange
                var msgRepoMock = new Mock<IMsgRepository>();
                msgRepoMock
                    .Setup(x => x.Get(1))
                    .ReturnsAsync(new MessageModel { Id = 1, UserId = 2 });

                var msgSvc = new MsgService(msgRepoMock.Object);

                // Act
                var response = await msgSvc.Delete(2, 1);

                // Assert
                response.Success.ShouldBeTrue();
                response.Error.ShouldBeNull();
            }

        }
    }
}
