using MsgBoard.Core.Helpers;
using Shouldly;
using System;
using Xunit;

namespace MsgBoard.UnitTests
{
    public class PasswordHasherTests
    {
        public class Verify
        {
            [Fact]
            public void Should_return_true_When_password_is_correct()
            {
                // Arrange
                var password = "Sesame open!";

                // Act
                (byte[] hash, byte[] salt) = PasswordHasher.Create(password);

                // Assert
                PasswordHasher.Verify(password, hash, salt).ShouldBeTrue();
            }

            [Fact]
            public void Should_return_false_When_password_is_wrong()
            {
                // Arrange
                var password = "Sesame open!";
                var faultyPassword = "sesame open!";

                // Act
                (byte[] hash, byte[] salt) = PasswordHasher.Create(password);

                // Assert
                PasswordHasher.Verify(faultyPassword, hash, salt).ShouldBeFalse();
            }
        }
    }
}
