# README #

This implementation is pretty much overkill as a solution to this assignment, but since it's been a
while since I last implemented a REST-API in .Net Core I took this as an oppertunity to refresh my knowledge,
and approach this assignment as if it was the beginning of a bigger project. This, I think, also gave me 
a chance to demonstrate a greater width of my knowledge.

By the way, I didn't do the API truly RESTFul, but then again few APIs seems to be.


## Project Structure ##

The solution is organized in the architectual style going by the name of Clean architecture or Onion architecture.
It's divided in 3 projects:

* MsgBoard.Api  
This is the REST-ish web API with dependencies to Core and Infrastructure. Purists will point out that
it's wrong for this project to depend on the Infrastructure-project, but it's only in Startup for setting
up the dependency injection, and the work it takes to make them truly separated just isn't worth the effort.

* MsgBoard.Core  
This is the home of interfaces, models and domain logic.

* MsgBoard.Infrastructure  
In this project this only contains the DatabaseContext and the repository-implementations.



## Prerequisites ##

This API is built in C# using Visual Studio 2019 and targeting .netcoreapp3.1. 

To build the project you need to install Visual Studio 2019. Download it here https://visualstudio.microsoft.com/downloads/


### Build ###

1. Start by cloning the repo:  
```git clone https://Jan_Adolfsson@bitbucket.org/Jan_Adolfsson/msgboard.git```  

2. You can build and run this either from Visual Studio or from the command line.

    **Visual Studio**  
From Visual Studio open the Solution found at msgboard/MsgBoard.Api/MsgBoard.Api.sln and start it. To be able to start it, you might have to set MsgBoard.Api-project as the startup project.

    **Command line**  
Open a command prompt and navigate to msgboard/MsgBoard.Api/MsgBoard.Api. Type:  
```dotnet run```
3. Browse to ```http://localhost:5000/api/swagger``` for API-documentation.


## Run locally ##

Use a tool like Postman or Isomnia for testing the API. And please refer to http://localhost:5000/api/swagger for API-documentation.

To post messages to the board you need to have a message board user. Add a user by a POST-request to api/users. Then authenticate your user by another POST-request to api/authenticate. The response will contain a bearer-token, make a copy of this for use in subsequent requests.

To use the token in a request you have to add a "Authorization"-header. Set the value as "Bearer" followed by a blank and your token.

By doing this you should be able to add and edit your own messages and view all messages.

## Run in container ##

In Powershell or other console:  
1. Navigate to folder msgBoard/MsgBoard.Api (where the Dockerfile is located)  
2. Type:  
    ```docker build -t msgboard .```  
3. Type:  
    ```docker run -it -rm -p 5000:80 --name msgboard msgboard```  
4. Browse to http://localhost:5000/swagger for API-documentation.  


### Known issues ###

The endpoint /api/users/{userid}/messages doesn't contain a check for if given userid exists.  

All get-endpoints return 400 when no resource is found, they really should return 404.   

The tests are way too few.  



### Stuff I didn't do ###

For a production API I would also consider following:

* Add versioning to endpoints.
* Add pagination parameters for get-endpoints.
* Store secret for auth in a more secure way.
* Add integration tests.
* Setup versioning and health endpoints.
* Improve logging.
* (Improve Swagger documentation.)
* (Return wether or not resources can be cached.)
* (Add navigation links to response bodies.)

